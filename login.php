<?php
session_start();
if (isset($_SESSION['user'])) {
    header("location:chat.php");
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $user = htmlspecialchars($_POST['username']);
    $pass = htmlspecialchars($_POST['password']);
    $con = mysqli_connect("wa.toad.cz", "savvavic", "webove aplikace", "savvavic");
    $query = "SELECT * FROM chatusers WHERE username = '$user'";
    $res = mysqli_query($con, $query);
    $num = mysqli_num_rows($res);
    if (empty($num)) {
        echo "<h4>No such user. Go back to try again.</h4>";
    }
    else {
        $row = mysqli_fetch_array($res, MYSQLI_ASSOC);
        if ($row && password_verify($_POST['password'], $row['password'])) {
            $_SESSION['user'] = $user;
            header("location: chat.php");
        } else {
            echo "<h4>Incorrect password. Go back to try again.</h4>";
        }
    }
}